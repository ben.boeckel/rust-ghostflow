# Checks

Checks may be used to enforce coding guidelines, formatting,
commit message standards, and more.

## Alphabetical list

  - [`allow_robot`](#allow_robot)
  - [`bad_commits`](#bad_commits)
  - [`changelog`](#changelog)
  - [`check_end_of_line`](#check_end_of_line)
  - [`check_executable_permissions`](#check_executable_permissions)
  - [`check_size`](#check_size)
  - [`check_whitespace`](#check_whitespace)
  - [`commit_subject`](#commit_subject)
  - [`fast_forward`](#fast_forward)
  - [`formatter`](#formatter)
  - [`formatting`](#formatting)
  - [`invalid_paths`](#invalid_paths)
  - [`invalid_utf8`](#invalid_utf8)
  - [`lfs_pointer`](#lfs_pointer)
  - [`reject_bidi`](#reject_bidi)
  - [`reject_binaries`](#reject_binaries)
  - [`reject_conflict_paths`](#reject_conflict_paths)
  - [`reject_merges`](#reject_merges)
  - [`reject_separate_root`](#reject_separate_root)
  - [`reject_symlinks`](#reject_symlinks)
  - [`release_branch`](#release_branch)
  - [`restricted_path`](#restricted_path)
  - [`submodule_available`](#submodule_available)
  - [`submodule_rewind`](#submodule_rewind)
  - [`submodule_watch`](#submodule_watch)
  - [`third_party`](#third_party)
  - [`valid_ghostflow_config`](#valid_ghostflow_config)
  - [`valid_name`](#valid_name)

## Content checks

Content checks may be used to check commits individually or to ensure that a
topic as a whole adheres to the check. For example, a per-commit check will
flag a commit which adds trailing whitespace even if a followup commit removes
it again whereas a topic check only sees the change as a whole against its
merge base.

### <a name="check_end_of_line"></a> `check_end_of_line`

Ensure that there is a newline at the end of text files.

  - *Rationale*: All lines in text files should end with an end-of-line marker
    including the last line in the file.
  - *Configuration*: None.
  - *Topic variant*: `check_end_of_line/topic`
  - *Workaround*: Mark the offending file as binary (using the `-diff`
    attribute).

### <a name="check_whitespace"></a> `check_whitespace`

Ensure that text files do not have whitespace errors. This includes trailing
whitespace and can include spaces and tab mismatches.

  - *Rationale*: Trailing whitespace is sometimes automatically removed in some
    editors and on collaborative projects, conflicts are common in such
    scenarios.
  - *Configuration*: See [`gitattributes(5)`][gitattributes-whitespace] and
    [`git-config(1)`][git-config-whitespace] for setting expected whitespace
    settings for files. The `conflict-marker-size` attribute may also be used
    to handle false positives in file formats which may contain Git's textual
    conflict markers incidentally.
  - *Topic variant*: `check_whitespace/topic`
  - *Workaround*: Set attributes appropriately for the file.

### <a name="formatter"></a> `formatter`

Ensure that files are formatted according to style guidelines or do not contain
errors from code linters or other static analysis.

Formatters have an associated `kind` which is used to detect files which should
be checked for formatting. Setting the `format.<kind>` attribute will mark the
file for formatting.

Note that formatter tools must abide by the rules described in the [`formatter
guidelines`][formatter-guidelines].

  - *Rationale*: Enforcing a coding style for formatting can reduce conflicts
    when backporting commits to release branches and commit noise to fix
    formatting errors. This may also be used to reject commits which contain
    linting errors.
  - *Configuration*:

```yaml
# Required. The formatter to lookup in the top-level formatter configuration.
kind: "formatter_name"
# Optional. The message to indicate how to fix the formatting errors.
fix_message: "Run `ghostflow-cli reformat`"
```

  - *Topic variant*: `formatter/topic`
  - *Workaround*: Unset the associated formatting attribute on the path.

### <a name="formatting"></a> `formatting`

Like the [`formatter`](#formatter) check, but does not use the global
`formatters` configuration lookup table.

  - *Rationale*: See `formatter`.
  - *Configuration*:

```yaml
# Optional (defaults to the value of `kind`). The name of the formatter (used
# in error messages).
name: "formatter name",
# Required. The name of the attribute to use to use this formatter.
kind: "kind",
# Required. The path to the formatter. May also be a command name which then
# requires it to be available in `PATH`.
formatter: "/path/to/formatter",
# Optional. A list of path patterns (see `gitignore(5)`) to check out for use
# by the formatting tool.
config_files:
    - .clang-format
# Optional. The message to indicate how to fix the formatting errors.
fix_message: "instructions for fixing",
# Optional. A timeout, in seconds, for running the formatter on each file.
timeout: 10,
```

  - *Topic variant*: `formatting/topic`
  - *Workaround*: Unset the associated formatting attribute on the path.

### <a name="invalid_utf8"></a> `invalid_utf8`

Check that text files contain only valid UTF-8 contents.

  - *Rationale*: UTF-8 is the *de facto* standard encoding today.
  - *Configuration*: None.
  - *Topic variant*: `invalid_utf8/topic`
  - *Workaround*: Mark the offending file as binary (using the `-diff`
    attribute).

### <a name="lfs_pointer"></a> `lfs_pointer`

Check that all files expected to be tracked by [`git-lfs`][git-lfs] are valid
LFS pointer files.

**Note**: This check does not check that the content pointed to by the LFS
pointer is available.

  - *Rationale*: Files using the `filter=lfs` attribute should be valid LFS
    pointers.
  - *Configuration*: None.
  - *Topic variant*: `lfs_pointer/topic`
  - *Workaround*: Remove the `filter=lfs` attribute from the file or fix the
    LFS pointer content to conform to the [LFS pointer
    spec][git-lfs-pointer-spec].

### <a name="reject_bidi"></a> `reject_bidi`

Reject Unicode bidirectional control characters in added content. See [Trojan
Codes][trojan-codes] for more information.

[trojan-codes]: https://www.trojansource.codes

  - *Rationale*: Unicode bidirectional control characters may be used to
    obsfuscate code during review where the rendering does not match what is
    actually seen by the compiler or interpreter.
  - *Configuration*:

```yaml
# Optional. Defaults to `false`. If `true`, changes are only reported as
# warnings rather than errors.
allow: false
```

  - *Topic variant*: `reject_bidi/topic`

### <a name="reject_binaries"></a> `reject_binaries`

Reject compiled binaries in the repository. Currently detected binaries:

  - `AR`: Static libraries on most platforms (including Windows).
  - `ELF`: Used on Linux, BSD, and other Unix platforms.
  - `Mach-O`: Used on macOS.
  - `PE`: Used on Windows for executables and DLLs.

More formats may be supported in the future.

  - *Rationale*: Compiled binaries should be part of a build system and
    generated rather than committed to the repository. In addition, binaries
    are not usually stored efficiently in Git's history.
  - *Configuration*: None.
  - *Topic variant*: `reject_binaries/topic`
  - *Workaround*: Set the `hooks-allow-binary` attribute for the file to the
    format of the file that is expected.

### <a name="reject_symlinks"></a> `reject_symlinks`

Check that symlinks are not present in the repository.

  - *Rationale*: Some platforms do not support symlinks and symlinks may be
    used to point to content outside of the repository which can pose security
    issues..
  - *Configuration*: None.
  - *Topic variant*: `reject_symlinks/topic`
  - *Workaround*: None.

### <a name="submodule_available"></a> `submodule_available`

Check that the commit pointed to by a submodule is available in the tracked
repository. It must also be reachable by the branch mentioned in the `branch`
setting for the module (which defaults to the submodule's default branch).

Additionally, the check can also enforce that the commit is on the first-parent
of the tracked branch. Without this enforcement, if the submodule points to a
non-first-parent commit, another commit which points to another
non-first-parent commit can have a non-trivial resolution when they are merged.
Requiring a first-parent commit ensures that there's a trivial conflict
resolution between the two submodule commits.

**Bug**: The special `branch = .` setting for the module is not supported (it
is treated as the submodule's default branch).

**Note**: This check can only work with the information that is locally
available in the submodule. In order to be accurate, the submodules should be
made up-to-date and have the local copies of the tracked ref match the remote
repository's ref.

  - *Rationale*: Submodule commits should always be available on the target
    repository. This ensure that the submodule may always be checked out no
    matter which commit of the parent repository is used (assuming that history
    is not deleted in the submodule's remote repository).
  - *Configuration*:

```yaml
# Optional. Defaults to `false`. Whether to require that the submodule point to
# a commit on first-parent history of the submodule's target branch.
require_first_parent: false
```

  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="submodule_rewind"></a> `submodule_rewind`

Check that when a submodule commit is changed, that the old commit is an
ancestor of the new commit.

  - *Rationale*: Submodules should generally move forward in time, not
    backwards.
  - *Configuration*: None.
  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="submodule_watch"></a> `submodule_watch`

Check for the addition and removal of submodules. By default, only warnings are
generated, but they may be elevated to warnings via the `reject_additions` and
`reject_removals` configuration settings.

**Bug**: Moving a submodule is not detected right now; it is seen as an
addition and a removal.

  - *Rationale*: Adding or removing a submodule is something deserving of more
    scrutiny during a review.
  - *Configuration*:

```yaml
# Optional. Defaults to `false`. Whether to error if a submodule is added.
reject_additions: false
# Optional. Defaults to `false`. Whether to error if a submodule is removed.
reject_removals: false
```

  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="valid_ghostflow_config"></a> `valid_ghostflow_config`

Validate that the `.ghostflow.yml` file is a valid configuration file.

  - *Rationale*: An invalid `.ghostflow.yml` file can cause checks to be
    skipped.
  - *Configuration*: None.
  - *Topic variant*: `valid_ghostflow_config/topic`
  - *Workaround*: None.

## File checks

### <a name="changelog"></a> `changelog`

Check that a changelog has been updated for a commit or topic. There are three
changelog styles supported:

  - `directory`: A directory containing changelog entries (generally one per
    file) is expected to be modified for each commit or topic. The files may
    optionally be required to have a specific extension.
  - `file`: A file is expected to be modified for each commit or topic.
  - `files`: A list of paths to changelog files. Any listed path having a
    modification is sufficient.

**Bug**: File changes only require a modification. Changing the mode (i.e., the
`+x` bit) is considered sufficient. In the future, a content change may be
required.

  - *Rationale*: Changelog entries being written with the original contribution
    can greatly improve the quality of the release notes for a release.
  - *Configuration*:

```yaml
# Required. The style of changelog used.
style: "style"
# Optional. Defaults to `false`. Whether to require a changelog change or just
# warn.
required: false

# `directory` style settings.
# Required. The path to the changelog directory.
path: "path/to/changelog/dir"
# Optional. Only files with the given extension are considered. If not
# specified, any file is sufficient.
extension: "ext"

# `file` style settings.
# Required. The path to the changelog file.
path: "path/to/changelog.file"

# `files` style settings.
# Required. The paths to the changelog files.
paths:
  - "path/to/changelog.file"
  - "path/to/other/changelog.file"
```

  - *Topic variant*: `changelog/topic`
  - *Workaround*: None.

### <a name="check_executable_permissions"></a> `check_executable_permissions`

Check that the `+x` permission bit matches the content of the file. The content
of the file is inspected to ensure that files which are marked as being
executable look like an executable and that those that appear to be executables
are marked as such.

Currently files with an extension specified in the `extensions` configuration
and files starting with a shebang (`#!/` or `#! /`) are considered as
should-be-executable. More formats may be detected in the future.

  - *Rationale*: Files which look executable (based on content) should have the
    executable permission bit set. Those which have it set should appear
    executable.
  - *Configuration*:

```yaml
# Optional. Extensions which indicate an executable. Used mainly for
# Windows-related executables.
extensions:
  - "bat"
  - "exe"
```

  - *Topic variant*: `check_executable_permissions/topic`
  - *Workaround*: None.

### <a name="check_size"></a> `check_size`

Check that files in the repository are smaller than a threshold size. The check
has a `max_size` configuration which is used as the default maximum size (in
bytes). The `hooks-max-size` attribute may be used to set per-file limits:

  - `-hooks-max-size`: Remove all limits.
  - `!hooks-max-size`: Use the default limit.
  - `hooks-max-size=<limit>`: Use a specified limit.

  - *Rationale*: Large files in the history never leave and can quickly inflate
    the size of a project's checkout.
  - *Configuration*:

```yaml
# Optional. Defaults to 1048576 (1 megabyte). The maximum size of files without
# other settings.
max_size: 1048576
```

  - *Topic variant*: `check_size/topic`
  - *Workaround*: Use the `hooks-max-size` attribute.

### <a name="invalid_paths"></a> `invalid_paths`

Check that all paths use a restricted character set. This aims to ensure
maximum compatibility of the repository itself across multiple platforms. All
non-ASCII bytes, whitespace, and ASCII control bytes are disallowed. In
addition, the characters specified in `invalid_characters` are also rejected.
As a special case, ASCII space (`SP`, 0x20) may be allowed using the
`allow_space` configuration.

**Todo**: Reject file names which are invalid on prevalent platforms as well.

  - *Rationale*: Not all platforms support all characters in paths.
  - *Configuration*:

```yaml
# Optional. Defaults to `<>:"|?*`. The set of characters to reject beyond the
# defaults.
invalid_characters: "<>:\"|?*"
# Optiona. Defaults to `false`. Whether to allow ` ` in paths or not.
allow_space: false
```

  - *Topic variant*: `invalid_paths/topic`
  - *Workaround*: None.

### <a name="reject_conflict_paths"></a> `reject_conflict_paths`

Check that conflict resolution files are not committed to the repository.

**Note**: The check tries to minimize false positives. Conflict files for files
which are deleted in the commit they were added in are not detected.

  - *Rationale*: Conflict files should not be committed to the history.
  - *Configuration*: None.
  - *Topic variant*: `reject_conflict_paths/topic`
  - *Workaround*: None.

### <a name="restricted_path"></a> `restricted_path`

Checks for modifications to specific paths within the project. Matches are done
on full paths. For example, restricting `path/to/file` will not trigger for
`path/to/file.ext`, but would for `path/to/dir/file` if restricting
`path/to/dir`.

  - *Rationale*: Some paths require a higher level of scrutiny during review,
    for example license files or changes to vendored code. This check can
    highlight changes to these files.
  - *Configuration*:

```yaml
# Required. The path to restrict.
restricted_path: "path/to/restricted/content"
# Optional. Defaults to `true`. If `false`, changes are only reported as
# warnings rather than errors.
required: true
```

  - *Topic variant*: `restricted_path/topic`
  - *Workaround*: None.

## History checks

### <a name="bad_commits"></a> `bad_commits`

Detect commits which are known to be invalid and reject their presence. This is
useful to reject old history for a rewritten repository.

  - *Rationale*: Commits which contained invalid code or are otherwise known to
    be invalid should be alerted about.
  - *Configuration*:

```yaml
bad_commits:
  - deadbeefdeadbeefdeadbeefdeadbeefdeadbeef
  - cafefeedcafefeedcafefeedcafefeedcafefeed
```

  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="fast_forward"></a> `fast_forward`

Detect when a proposed merge is not eligible for a fast-forward merge. Note
that this check is temporal in nature because the target branch may move since
the check has been last performed. Note that (barring force pushes), a failure
will not change until the topic is rebased.

  - *Rationale*: Workflows which use a fast-forward merge strategy exist.
  - *Configuration*:

```yaml
# Required: the name of the branch to check for fast-forward status.
branch: "release"
# Optional. Defaults to `false`. If `false`, changes are only reported as
# warnings rather than errors.
required: true
```

### <a name="reject_merges"></a> `reject_merges`

Detect and reject merge commits in topics.

  - *Rationale*: Some workflows do not accept merges in reviewed code.
  - *Configuration*: None.
  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="reject_separate_root"></a> `reject_separate_root`

Check that all commits are associated with the existing project's history.

  - *Rationale*: New root commits indicate that a completely separate history
    has been merged. This is rarely desired.
  - *Configuration*: None.
  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="release_branch"></a> `release_branch`

Check that changes for the topic are eligible for the branch. This can ensure
that branches tracking releases do not include post-release changes.

  - *Rationale*: Release branches should contain minimal changes. Bringing in
    large changes can break release stability guarantees.
  - *Configuration*:

```yaml
# Optional. Defaults to `release`. The name of the release branch. Note that
# this is not the Git name of the branch and is only used to name the branch in
# the error message.
branch: "release"
# Required. The first commit which is not allowed on the branch. Usually this
# is the first commit on the source branch after the release branch has been
# created.
disallowed_commit: "deadbeefdeadbeefdeadbeefdeadbeefdeadbeef"
# Optional. Defaults to `false`.
required: false
```

  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="third_party"></a> `third_party`

Checks that modifications to a third party import directory follow the Kitware
third party import pattern.

The structure of the import is:

```
A --- B --- C
     /     /
    R --- U
```

where:

  - `A`, `B`, and `C` are commits in the main project.
  - `R` is the root commit for the import tracking branch.
  - `U` is an update to the import.

The import branch (the history of `U`) is a series of snapshots of the history
of the import repository (referred to in messages using the `name`
configuration). This history is merged into the main project using the
[`-Xsubtree=<path>`][git-merge-subtree-strategy] into the configured `path`. It
enforces that any modifications to this directory in the main history comes in
through the associated import history (tracked using the `root` configuration).
It also ensures that the subtree is not modified in the merge commit (i.e.,
"evil merges" are not allowed). The strategy uses snapshots, so conflicts
(which are indistinguishable from "evil merges") should never happen.

The `script` configuration is used to indicate the proper update procedure to
update the tracked directory.

**Note**: The `script` argument may be deprecated in the future if
`ghostflow-cli` gains support for performing the update procedure as a
subcommand.

  - *Rationale*: Projects using the third party tracking strategy expect that
    their third party code is tracked appropriately.
  - *Configuration*:

```yaml
name: "extlib"
path: "path/to/extlib/import"
root: "deadbeefdeadbeefdeadbeefdeadbeefdeadbeef"
script: "update instructions"
```

  - *Topic variant*: None.
  - *Workaround*: None.

## Metadata checks

### <a name="allow_robot"></a> `allow_robot`

Ignore the results of other checks for topics authored by the indicated user.
If this check detects a topic contributed by the indicated user, error messages
are not enforced.

**Note**: Local checks cannot truly verify the user's identity. This is mostly
useful for checks triggered through hosted infrastructure where the author of a
contribution can be determined using an API.

  - *Rationale*: Some changes which would otherwise be disallowed can bypass
    the checks by using a specific user with elevated permissions on the
    project.
  - *Configuration*:

```yaml
# Required. The name of the robot's identity.
name: "Robot Name"
# Required. The email address of the robot's identity.
email: "robot@email.invalid"
```

  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="commit_subject"></a> `commit_subject`

Check that commit subjects adhere to the basic commit message guidelines. The
structure of commit messages is assumed to be:

```
Commit subject

Commit message body
```

The following guidelines are enforced:

  - Commit subjects must not be too short. These are not informative and are
    the only information when viewing history overviews in many tools. See the
    `min_length` configuration.
  - Commit subjects must not be too long. Commit subjects should be short and a
    summary of what happens in the commit. Overlong subjects should either
    place details into the commit message body or they do too many things and
    should be split into multiple commits. See the `max_length` configuration.
    Commit subjects which match Git's generated subjects (e.g., merges and
    reverts) are not checked here.
  - Empty second line. If a commit message has a second line, it must be empty.
    It also must be followed by a non-empty commit message body.
  - Work-in-progress commits (those with subjects prefixed with `WIP` or
    `Draft` in a vew different common forms) are rejected. Before integration,
    work-in-progress commits should be completed and the commit message updated
    to reflect the status change. See the `check_wip` configuration.
  - Git's `rebase` uses commit subjects to implement the
    [autosquash feature][git-rebase-autosquash]. Commits using these should be
    rebased before integration. See the `check_rebase_commands` configuration.

Some workflows enforce that their commit message subjects start with specific
prefixes. These prefixes may be allowed or disallowed using the
`allowed_prefixes` and `disallowed_prefixes` configurations. For commit
messages which might follow other workflow processes but shouldn't be used
normally, the `tolerated_prefixes` configuration may use [regular
expressions][regular-expression-syntax] to match commit messages which can
ignore the explicit `allowed_prefixes` and `disallowed_prefixes` configuration.

  - *Rationale*: Commit messages are important for understanding the history of
    a project. This check enforces the basic guidelines for good commit
    messages.
  - *Configuration*:

```yaml
# Optional. Defaults to `8`. The minimum length of commit message subjects.
min_length: 8
# Optional. Defaults to `78`. The maximum length of commit message subjects.
max_length: 78
# Optional. Defaults to `true`. Reject commit messages starting with `WIP` or
# `wip`.
check_wip: true
# Optional. Defaults to `true`. Reject commit messages which contain rebase
# commands.
check_rebase_commands: true
# Optional. Defaults to `false`. Reject commit messages which match messages
# generated automatically through suggestion application in common forges.
check_suggestion_subjects: true
# Optional. Regular expressions which match prefixes for commit message
# subjects which are allowed.
tolerated_prefixes:
  - "regex"
# Optional. Literal prefixes which must be used to begin commit message
# subjects.
allowed_prefixes:
  - "literal"
# Optional. Literal prefixes which may not be used to begin commit message
# subjects.
disallowed_prefixes:
  - "literal"
```

  - *Topic variant*: None.
  - *Workaround*: None.

### <a name="valid_name"></a> `valid_name`

Check that commit authors and committers have a full name and plausibly valid
email address. Full names are detected by checking for an ASCII space (SP,
0x20) in the name portion of the associated identity. Email addresses are
checked for the basic `user@host` format and that the host responds contains an
`MX` DNS record. Host responses are stored in a cache to reduce network
traffic. Names can be enforced with the `policy` configuration:

  - `required`: Full names are required.
  - `preferred`: Full names are only warned about.
  - `optional`: Names are not checked at all.

**Note**: Not all cultures use spaces to separate the first and last names, so
this check can be overly strict in such environments.

**Note**: This check assumes that the [`host`][host-man-page] from [the BIND
project][bind-project] is available on the system in `$PATH`.

  - *Rationale*: Projects may require legal names for contributions.
  - *Configuration*:

```yaml
# Optional. Defaults to `required`. The policy for checking for full names.
full_name_policy: "required"
# Optional. Domains which can be assumed to be valid. These can avoid spurious
# errors from DNS lookup errors.
trust_domains:
  - "domain.invalid"
```

  - *Topic variant*: None.
  - *Workaround*: None. DNS failures may be spurious and triggering the checks
    again can resolve errors.

[bind-project]: https://www.isc.org/downloads/bind/
[formatter-guidelines]: ../formatters/index.html
[git-config-whitespace]: https://git-scm.com/docs/git-config#Documentation/git-config.txt-corewhitespace
[git-lfs-pointer-spec]: https://github.com/git-lfs/git-lfs/blob/master/docs/spec.md
[git-lfs]: https://git-lfs.github.com/
[git-merge-subtree-strategy]: https://git-scm.com/docs/git-merge#Documentation/git-merge.txt-subtreeltpathgt
[git-rebase-autosquash]: https://git-scm.com/docs/git-rebase#Documentation/git-rebase.txt---autosquash
[gitattributes-whitespace]: https://git-scm.com/docs/gitattributes#_checking_whitespace_errors
[host-man-page]: https://linux.die.net/man/1/host
[regular-expression-syntax]: https://docs.rs/regex/1/regex/#syntax
