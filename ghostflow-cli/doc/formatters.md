# Formatter guidelines

Tools are expected to behave in certain ways for the formatters. For checkers
and formatters, they are generally the same with a few minor differences,
discussed below.

## Fail-safe

Formatters cannot assume much about the environment they are run inside. The
only thing that is given by the code which runs formatters is a path to the
file to format as its first argument and a working directory analogous to the
top-level directory of the project it is running within.

The file *should* exist, but something may have failed in making it available.
In addition, any configuration files required by the formatter should be
checked for as well.

## File modification

A poorly-formatted file is detected by a modification to the path it was given
as an argument. This means that tools should be directed to perform in-place
modification if possible. For tools which are primarily used as checks rather
than actual reformatters, it is generally best to have them append their output
to the input file.

## Exit code

A successful error code is required in order for the formatter to be considered
to have succeeded. This is generally a problem for checkers rather than
reformatters, so their exit codes should be isolated from the actual formatter.

## Leftover files

Formatters should not create new files such as backup files, logs, etc. If
tools cannot be made to not create these files, the formatter wrapper will need
to remove them.

## No VCS access

Formatters are run inside of a constructed VCS tree which may not work as
expected in all cases. Even though they may technically be inside of a Git
repository, the environment is not set up to use it properly. This is because
the setup is intended to minimize file system access where possible for
performance reasons.

## Idempotent

Formatters should be idempotent. This means that running a formatter over a
file multiple times should change the content of the file at most once.
However, this is generally not as much a function of the formatter, but of the
tool which is used to do the formatting. Ideally, these would be considered
bugs in the tool and fixed.

This can be mitigated by formatting the entire repository in a single sweep
before using formatters are part of the workflow. Individual topics usually do
not change so much code that they trigger these cases.

# Reformatters

When used as checks, operations are safe and bugs are more tolerable. However,
when they are used as actual reformatters, history for topics is being actively
re-written and pushed. Since formatters can generally do anything to the
contents of a file, modifications to the file (other than deletion, which is an
error) are trusted implicitly.

This means that formatters used in this manner should be thoroughly tested.
External tools are generally fine for this, but custom formatting logic should
be verified across the codebases in which they are being used and over time to
ensure that issues should be rare.
