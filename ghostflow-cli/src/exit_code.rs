// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::process;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ExitCode {
    Success,
    Failure,
}

pub enum Never {}

impl ExitCode {
    pub fn exit(self) -> Never {
        let code = match self {
            ExitCode::Success => 0,
            ExitCode::Failure => 1,
        };

        process::exit(code)
    }
}
