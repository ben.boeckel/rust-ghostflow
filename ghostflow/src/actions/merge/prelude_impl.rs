// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

pub use crate::actions::merge::MergeError;
pub(crate) use crate::actions::merge::MergeResult;

pub use crate::actions::merge::policy::MergePolicy;

pub use crate::actions::merge::settings::MergeActionResult;
pub use crate::actions::merge::settings::MergeInformation;
pub use crate::actions::merge::settings::MergeSettings;
pub use crate::actions::merge::settings::Merger;
