// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Testing actions
//!
//! Different projects have different strategies for proposing topics for testing. The different
//! strategies are implemented as actions here.

pub mod jobs;
pub mod pipelines;
pub mod refs;
