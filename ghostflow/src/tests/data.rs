// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::fs::{self, Permissions};
use std::io::Write;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::process::{Command, Stdio};

use git_workarea::GitContext;

use crate::actions::data::*;
use crate::host::Repo;
use crate::tests::utils::test_workspace_dir;

struct TestRepo {
    pub ctx: GitContext,
    pub repo: Repo,
}

impl TestRepo {
    fn new(ctx: GitContext) -> Self {
        TestRepo {
            repo: Repo {
                name: "test_repo".into(),
                url: ctx.gitdir().to_string_lossy().into_owned(),
                forked_from: None,
            },
            ctx,
        }
    }
}

fn git_context(workspace_path: &Path) -> (TestRepo, GitContext) {
    // Here, we create two clones of the current repository: one to act as the remote and another
    // to be the repository the data action uses. The first is cloned from the source tree's
    // directory while the second is cloned from that first clone. This sets up the `origin` remote
    // properly for testing the `data` command.

    let origindir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/../.git"))
        .arg(&origindir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!(
            "origin clone failed: {}",
            String::from_utf8_lossy(&clone.stderr),
        );
    }

    let gitdir = workspace_path.join("git");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(&origindir)
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!(
            "working clone failed: {}",
            String::from_utf8_lossy(&clone.stderr),
        );
    }

    (
        TestRepo::new(GitContext::new(origindir)),
        GitContext::new(gitdir),
    )
}

fn create_data<D>(git: &GitContext, destination: D) -> Data
where
    D: AsRef<Path>,
{
    let mut data = Data::new(git.clone());
    data.add_destination(destination.as_ref().to_string_lossy());
    data
}

fn data_ref_exists(ctx: &GitContext, namespace: &str, algo: &str, hash: &str) -> bool {
    ctx.git()
        .arg("show-ref")
        .arg("--quiet")
        .arg("--verify")
        .arg(format!("refs/{}/{}/{}", namespace, algo, hash))
        .status()
        .unwrap()
        .success()
}

fn make_data_object(ctx: &GitContext, namespace: &str, contents: &[u8], algo: &str, hash: &str) {
    let mut hash_object = ctx
        .git()
        .arg("hash-object")
        .arg("-w")
        .args(&["-t", "blob"])
        .arg("--stdin")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .unwrap();
    hash_object
        .stdin
        .as_mut()
        .unwrap()
        .write_all(contents)
        .unwrap();
    let hash_object = hash_object.wait_with_output().unwrap();
    if !hash_object.status.success() {
        panic!(
            "hash object creation failed: {}",
            String::from_utf8_lossy(&hash_object.stderr),
        );
    }
    let object = String::from_utf8_lossy(&hash_object.stdout);

    let refname = format!("refs/{}/{}/{}", namespace, algo, hash);
    let update_ref = ctx
        .git()
        .arg("update-ref")
        .arg(&refname)
        .arg(object.trim())
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!(
            "updating the data ref {} failed: {}",
            refname,
            String::from_utf8_lossy(&update_ref.stderr),
        );
    }
    assert!(data_ref_exists(ctx, namespace, algo, hash));
}

const VALID_DATA: &[(&str, &str)] = &[
    ("MD5", "eb733a00c0c9d336e65691a37ab54293"),
    (
        "SHA256",
        "916f0027a575074ce72a331777c3478d6513f786a591bd892da1a577bf2335f9",
    ),
    (
        "SHA512",
        "0e1e21ecf105ec853d24d728867ad70613c21663a4693074b2a3619c1bd39d66\
         b588c33723bb466c72424e80e3ca63c249078ab347bab9428500e7ee43059d0d",
    ),
];

const INVALID_DATA: &[(&str, &str)] = &[
    ("MD5", "notahash"),
    ("SHA256", "notahash"),
    ("SHA512", "notahash"),
];

fn create_data_refs(data: &[(&str, &str)], ctx: &GitContext, namespace: &str) {
    let contents = b"test data";

    data.iter()
        .for_each(|&(algo, hash)| make_data_object(ctx, namespace, contents, algo, hash));
}

fn check_data_refs(data: &[(&str, &str)], ctx: &GitContext, namespace: &str, keep_refs: bool) {
    data.iter().for_each(|&(algo, hash)| {
        assert_eq!(data_ref_exists(ctx, namespace, algo, hash), keep_refs);
    });
}

fn check_data<D>(data: &[(&str, &str)], destination: D)
where
    D: AsRef<Path>,
{
    let path = destination.as_ref();
    data.iter().for_each(|&(algo, hash)| {
        let data_file = path.join(algo).join(hash);
        assert!(data_file.exists());
        assert!(fs::metadata(data_file).unwrap().permissions().readonly());
    });
}

// Repositories without data should do nothing.
#[test]
fn test_no_data() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let data = create_data(&ctx, destination);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::NoData);
}

// No destination in the action should fail.
#[test]
fn test_no_destinations() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let data = Data::new(ctx);

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::NoDestinations);
}

// No data shouldn't care if the destinations is empty.
#[test]
fn test_no_destinations_no_data() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let data = Data::new(ctx);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::NoData);
}

// Repositories with data should appear in the destination.
#[test]
fn test_data_sync() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let data = create_data(&ctx, &destination);

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, "data", false);
    check_data_refs(VALID_DATA, &ctx, "data", false);
}

// Repositories with data should appear in multiple destinations.
#[test]
fn test_data_sync_multiple() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let destination2 = tempdir.path().join("destination2");
    let mut data = create_data(&ctx, &destination);

    data.add_destination(destination2.to_string_lossy());

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data(VALID_DATA, &destination2);
    check_data_refs(VALID_DATA, &test_repo.ctx, "data", false);
    check_data_refs(VALID_DATA, &ctx, "data", false);
}

// Repositories with data should appear in the destination with the right namespace.
#[test]
fn test_data_sync_namespace() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    let namespace = "data-alternate";
    data.ref_namespace(namespace);

    create_data_refs(VALID_DATA, &test_repo.ctx, namespace);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, namespace, false);
    check_data_refs(VALID_DATA, &ctx, namespace, false);
}

// Repositories with data should appear in the destination with the right namespace even if the
// namespace has a slash in it.
#[test]
fn test_data_sync_namespace_subdir() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    let namespace = "data-alternate/with/subdir";
    data.ref_namespace(namespace);

    create_data_refs(VALID_DATA, &test_repo.ctx, namespace);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, namespace, false);
    check_data_refs(VALID_DATA, &ctx, namespace, false);
}

// Repositories with data should appear in the destination, but refs should be allowed to stay.
#[test]
fn test_data_sync_keep_refs() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    data.keep_refs();

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, "data", true);
    check_data_refs(VALID_DATA, &ctx, "data", true);
}

// Repositories with data should appear in the destination, but refs should be allowed to stay.
#[test]
fn test_data_sync_keep_refs_namespace() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    let namespace = "data-alternate";
    data.keep_refs().ref_namespace(namespace);

    create_data_refs(VALID_DATA, &test_repo.ctx, namespace);

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data(VALID_DATA, &destination);
    check_data_refs(VALID_DATA, &test_repo.ctx, namespace, true);
    check_data_refs(VALID_DATA, &ctx, namespace, true);
}

// Repositories with data should appear in the destination.
#[test]
fn test_data_sync_hash_mismatch() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let data = create_data(&ctx, &destination);

    create_data_refs(INVALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data_refs(INVALID_DATA, &test_repo.ctx, "data", false);
    check_data_refs(INVALID_DATA, &ctx, "data", false);
}

// Invalid data should never be kept locally, but remote should be allowed to stay.
#[test]
fn test_data_sync_hash_mismatch_keep_data() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination = tempdir.path().join("destination");
    let mut data = create_data(&ctx, &destination);
    data.keep_refs();

    create_data_refs(INVALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo).unwrap();
    assert_eq!(res, DataActionResult::DataPushed);

    check_data_refs(INVALID_DATA, &test_repo.ctx, "data", true);
    check_data_refs(INVALID_DATA, &ctx, "data", false);
}

fn is_root() -> bool {
    if cfg!(unix) {
        let uid = unsafe { libc::getuid() };

        uid == 0
    } else {
        false
    }
}

// Failure to sync should fail safely.
#[test]
fn test_data_sync_fail() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination_parent = tempdir.path().join("destination");
    let rsync_destination = destination_parent.join("child");
    let data = create_data(&ctx, &rsync_destination);
    fs::create_dir_all(&destination_parent).unwrap();
    fs::set_permissions(&destination_parent, Permissions::from_mode(0o555)).unwrap();

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo);

    if is_root() {
        println!("test_data_sync_fail is not effective as `root`");
        let _ = res.unwrap();
    } else {
        let err = res.unwrap_err();

        if let DataError::Rsync {
            destination,
            status,
            ..
        } = err
        {
            assert_eq!(Path::new(&destination), rsync_destination);
            assert_eq!(status, Some(11));
        } else {
            panic!("unexpected error: {:?}", err);
        }

        check_data_refs(VALID_DATA, &test_repo.ctx, "data", false);
        check_data_refs(VALID_DATA, &ctx, "data", true);
    }
}

// Failure to sync should fail safely even if a destination fails.
#[test]
fn test_data_sync_fail_multiple() {
    let tempdir = test_workspace_dir();
    let (test_repo, ctx) = git_context(tempdir.path());
    let destination_parent = tempdir.path().join("destination");
    let rsync_destination = destination_parent.join("child");
    let rsync_destination2 = tempdir.path().join("destination2");
    let mut data = create_data(&ctx, &rsync_destination);
    data.add_destination(rsync_destination2.to_string_lossy());
    fs::create_dir_all(&destination_parent).unwrap();
    fs::set_permissions(&destination_parent, Permissions::from_mode(0o555)).unwrap();

    create_data_refs(VALID_DATA, &test_repo.ctx, "data");

    let res = data.fetch_data(&test_repo.repo);

    if is_root() {
        println!("test_data_sync_fail_multiple is not effective as `root`");
        let _ = res.unwrap();
    } else {
        let err = res.unwrap_err();

        if let DataError::Rsync {
            destination,
            status,
            ..
        } = err
        {
            assert_eq!(Path::new(&destination), rsync_destination);
            assert_eq!(status, Some(11));
        } else {
            panic!("unexpected error: {:?}", err);
        }

        check_data(VALID_DATA, &rsync_destination2);

        check_data_refs(VALID_DATA, &test_repo.ctx, "data", false);
        check_data_refs(VALID_DATA, &ctx, "data", true);
    }
}
