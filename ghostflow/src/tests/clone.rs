// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::fs::read_link;
use std::path::{Path, PathBuf};
use std::sync::Arc;

use crate::actions::clone::*;
use crate::host::{HostedProject, HostingService};
use crate::tests::mock::{MockData, MockService};
use crate::tests::utils::{check_git_config_value, check_git_config_values, test_workspace_dir};

const PROJECT_NAME: &str = "self";

fn create_clone(path: &Path, service: &Arc<MockService>) -> Clone_ {
    let project = HostedProject {
        name: PROJECT_NAME.into(),
        service: Arc::clone(service) as Arc<dyn HostingService>,
    };

    Clone_::new(path, project)
}

#[test]
fn test_clone_watched_repo() {
    let tempdir = test_workspace_dir();
    let service = MockData::builder()
        .add_project(MockData::self_repo())
        .service();
    let clone = create_clone(tempdir.path(), &service);
    assert!(!clone.exists());

    let ctx = clone.clone_watched_repo().unwrap();

    check_git_config_value(&ctx, "remote.origin.tagopt", "--no-tags");
    check_git_config_value(
        &ctx,
        "remote.origin.url",
        &service.repo(PROJECT_NAME).unwrap().url,
    );
    check_git_config_value(&ctx, "core.logAllRefUpdates", "true");

    let clone_after = create_clone(tempdir.path(), &service);
    assert!(clone_after.exists());

    let ctx = clone_after.clone_watched_repo().unwrap();

    check_git_config_value(&ctx, "remote.origin.tagopt", "--no-tags");
    check_git_config_value(
        &ctx,
        "remote.origin.url",
        &service.repo(PROJECT_NAME).unwrap().url,
    );
    check_git_config_value(&ctx, "core.logAllRefUpdates", "true");
}

#[test]
fn test_clone_watched_repo_submodules() {
    let tempdir = test_workspace_dir();
    let service = MockData::builder()
        .add_project(MockData::self_repo())
        .service();
    let mut clone = create_clone(tempdir.path(), &service);
    let internal = "submodule";
    let external = "/dev";
    clone.with_submodule("internal", CloneSubmoduleLink::new(internal));
    clone.with_submodule("external", CloneSubmoduleLink::new(external));
    clone.with_submodule("subdir/internal", CloneSubmoduleLink::new(internal));
    clone.with_submodule("subdir/external", CloneSubmoduleLink::new(external));
    assert!(!clone.exists());

    let ctx = clone.clone_watched_repo().unwrap();

    assert_eq!(
        read_link(ctx.gitdir().join("modules").join("internal")).unwrap(),
        tempdir.path().join(format!("{}.git", internal)),
    );
    assert_eq!(
        read_link(ctx.gitdir().join("modules").join("external")).unwrap(),
        PathBuf::from(external),
    );
    assert_eq!(
        read_link(ctx.gitdir().join("modules").join("subdir/internal")).unwrap(),
        tempdir.path().join(format!("{}.git", internal)),
    );
    assert_eq!(
        read_link(ctx.gitdir().join("modules").join("subdir/external")).unwrap(),
        PathBuf::from(external),
    );
}

#[test]
fn test_clone_watched_repo_prune() {
    let tempdir = test_workspace_dir();
    let service = MockData::builder()
        .add_project(MockData::self_repo())
        .service();
    let clone = create_clone(tempdir.path(), &service);
    assert!(!clone.exists());

    let ctx = clone.clone_watched_repo().unwrap();

    check_git_config_value(&ctx, "remote.origin.tagopt", "--no-tags");
    check_git_config_value(
        &ctx,
        "remote.origin.url",
        &service.repo(PROJECT_NAME).unwrap().url,
    );
    check_git_config_value(&ctx, "core.logAllRefUpdates", "true");

    let refname = "refs/heads/test-branch-test_clone_watched_repo_prune";
    let update_ref = ctx
        .git()
        .arg("update-ref")
        .arg(refname)
        .arg("83d9837adde715c471d0e3292e53284098c748f4")
        .arg("0000000000000000000000000000000000000000")
        .output()
        .unwrap();
    if !update_ref.status.success() {
        panic!(
            "update-ref failed: {}",
            String::from_utf8_lossy(&update_ref.stderr),
        );
    }

    let clone_after = create_clone(tempdir.path(), &service);
    assert!(clone_after.exists());

    let ctx = clone_after.clone_watched_repo().unwrap();

    check_git_config_value(&ctx, "remote.origin.tagopt", "--no-tags");
    check_git_config_value(
        &ctx,
        "remote.origin.url",
        &service.repo(PROJECT_NAME).unwrap().url,
    );
    check_git_config_value(&ctx, "core.logAllRefUpdates", "true");

    let rev_parse = ctx
        .git()
        .arg("rev-parse")
        .arg("--verify")
        .arg("--quiet")
        .arg(refname)
        .output()
        .unwrap();
    if rev_parse.status.success() {
        panic!("expected to prune the {} refname upon a clone", refname);
    }
}

#[test]
fn test_clone_mirror_repo() {
    let tempdir = test_workspace_dir();
    let service = MockData::builder()
        .add_project(MockData::self_repo())
        .service();
    let clone = create_clone(tempdir.path(), &service);
    assert!(!clone.exists());

    let refs = ["refs/heads/*", "refs/tags/*"];
    let ctx = clone.clone_mirror_repo(&refs).unwrap();

    check_git_config_value(
        &ctx,
        "remote.origin.url",
        &service.repo(PROJECT_NAME).unwrap().url,
    );
    check_git_config_value(&ctx, "core.logAllRefUpdates", "true");
    check_git_config_values(
        &ctx,
        "remote.origin.fetch",
        &["+refs/heads/*:refs/heads/*", "+refs/tags/*:refs/tags/*"],
    );

    let clone_after = create_clone(tempdir.path(), &service);
    assert!(clone_after.exists());

    let ctx = clone_after.clone_mirror_repo(&refs).unwrap();

    check_git_config_value(
        &ctx,
        "remote.origin.url",
        &service.repo(PROJECT_NAME).unwrap().url,
    );
    check_git_config_value(&ctx, "core.logAllRefUpdates", "true");
    check_git_config_values(
        &ctx,
        "remote.origin.fetch",
        &["+refs/heads/*:refs/heads/*", "+refs/tags/*:refs/tags/*"],
    );

    let clone_change = create_clone(tempdir.path(), &service);
    assert!(clone_change.exists());

    let new_refs = ["refs/heads/*", "refs/tags/*", "refs/notes/*"];
    let ctx = clone_change.clone_mirror_repo(&new_refs).unwrap();

    check_git_config_value(
        &ctx,
        "remote.origin.url",
        &service.repo(PROJECT_NAME).unwrap().url,
    );
    check_git_config_value(&ctx, "core.logAllRefUpdates", "true");
    check_git_config_values(
        &ctx,
        "remote.origin.fetch",
        &[
            "+refs/heads/*:refs/heads/*",
            "+refs/tags/*:refs/tags/*",
            "+refs/notes/*:refs/notes/*",
        ],
    );
}
