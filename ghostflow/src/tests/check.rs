// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::path::Path;
use std::process::Command;
use std::sync::Arc;

use git_checks_core::GitCheckConfiguration;
use git_workarea::{CommitId, GitContext};

use crate::actions::check::*;
use crate::host::{CommitStatusState, HostingService};
use crate::tests::mock::{MockData, MockMergeRequest, MockService};
use crate::tests::utils::test_workspace_dir;

const REPO_NAME: &str = "base";
const MR_ID: u64 = 1;
const MR_CONFLICT_ID: u64 = 2;
const MR_UPDATE_ID: u64 = 3;
const MR_MISSED_UPDATE_ID: u64 = 4;
const MR_WIP_ID: u64 = 5;
const MR_ALREADY_MERGED_ID: u64 = 6;
const COMMIT: &str = "7189cf557ba2c7c61881ff8669158710b94d8df1";
const COMMIT_CONFLICT: &str = "755842266dcc5739c06d61433241f44b9306f24c";
const COMMIT_UPDATE: &str = "fe70f127605efb6032cacea0bd336428d67ed5a3";
const COMMIT_MISSED_UPDATE: &str = "1b340d2edcf19077ab3e27ddda7430a6612c2f62";
const COMMIT_WIP: &str = "f6f8de8c7c5f1a081b14f5a47c7798268f383222";
const COMMIT_ALREADY_MERGED: &str = "6d60de8086a5eaa0350a4a9d6c1897cfdc49f479";

mod test_checks {
    use std::error::Error;

    use git_checks_core::impl_prelude::*;

    #[derive(Debug, Default, Clone, Copy)]
    pub struct Allow;

    impl TopicCheck for Allow {
        fn name(&self) -> &str {
            "allow"
        }

        fn check(&self, _: &CheckGitContext, _: &Topic) -> Result<CheckResult, Box<dyn Error>> {
            let mut result = CheckResult::new();

            result.whitelist();

            Ok(result)
        }
    }

    #[derive(Debug, Default, Clone, Copy)]
    pub struct Temporary;

    impl Check for Temporary {
        fn name(&self) -> &str {
            "temporary"
        }

        fn check(
            &self,
            _: &CheckGitContext,
            commit: &Commit,
        ) -> Result<CheckResult, Box<dyn Error>> {
            let mut result = CheckResult::new();

            result
                .add_warning(format!("A temporary warning for commit {}.", commit.sha1))
                .make_temporary();

            Ok(result)
        }
    }

    #[derive(Debug, Default, Clone, Copy)]
    pub struct AlwaysWarn;

    impl Check for AlwaysWarn {
        fn name(&self) -> &str {
            "always-warn-commit"
        }

        fn check(
            &self,
            _: &CheckGitContext,
            commit: &Commit,
        ) -> Result<CheckResult, Box<dyn Error>> {
            let mut result = CheckResult::new();

            result.add_warning(format!("This warning shows up for commit {}.", commit.sha1));

            Ok(result)
        }
    }

    impl BranchCheck for AlwaysWarn {
        fn name(&self) -> &str {
            "always-warn-branch"
        }

        fn check(&self, _: &CheckGitContext, _: &CommitId) -> Result<CheckResult, Box<dyn Error>> {
            let mut result = CheckResult::new();

            result.add_warning("This warning shows up for every branch.");

            Ok(result)
        }
    }
}

const BASE: &str = "58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd";

fn git_context(workspace_path: &Path) -> GitContext {
    let gitdir = workspace_path.join("origin");
    let clone = Command::new("git")
        .arg("clone")
        .arg("--bare")
        .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/../.git"))
        .arg(&gitdir)
        .output()
        .unwrap();
    if !clone.status.success() {
        panic!(
            "origin clone failed: {}",
            String::from_utf8_lossy(&clone.stderr),
        );
    }

    GitContext::new(gitdir)
}

fn admins() -> Vec<String> {
    vec!["admin".into(), "admin2".into()]
}

fn check_service() -> Arc<MockService> {
    let base = MockData::repo(REPO_NAME);
    let user = MockData::user("user");
    MockData::builder()
        .add_merge_request(MockMergeRequest::new(MR_ID, &user, COMMIT, &base, &base))
        .add_merge_request(MockMergeRequest::new(
            MR_CONFLICT_ID,
            &user,
            COMMIT_CONFLICT,
            &base,
            &base,
        ))
        .add_merge_request(MockMergeRequest::new(
            MR_UPDATE_ID,
            &user,
            COMMIT_UPDATE,
            &base,
            &base,
        ))
        .add_merge_request(MockMergeRequest::new(
            MR_MISSED_UPDATE_ID,
            &user,
            COMMIT_MISSED_UPDATE,
            &base,
            &base,
        ))
        .add_merge_request(
            MockMergeRequest::new(MR_WIP_ID, &user, COMMIT_WIP, &base, &base).work_in_progress(),
        )
        .add_merge_request(MockMergeRequest::new(
            MR_ALREADY_MERGED_ID,
            &user,
            COMMIT_ALREADY_MERGED,
            &base,
            &base,
        ))
        .add_project(base)
        .add_user(user)
        .service()
}

// A successful check should not comment, but post a success status on the branch and each commit.
#[test]
fn test_check_success() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();
    let config = GitCheckConfiguration::new();

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let result = check
        .check_mr("test_check_success", &CommitId::new(BASE), &mr)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_commit_statuses_head = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses_head.len(), 2);

    let status = &mr_commit_statuses_head[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_commit_statuses_head[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);
}

// Checks should support a custom basename for statuses.
#[test]
fn test_check_custom_base_name() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();
    let config = GitCheckConfiguration::new();

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins).base_name("test");

    let result = check
        .check_mr("test_check_custom_base_name", &CommitId::new(BASE), &mr)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_commit_statuses_head = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses_head.len(), 2);

    let status = &mr_commit_statuses_head[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "test-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_commit_statuses_head[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "test-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);
}

// A successful check should not comment or send commit statuses if it is quiet.
#[test]
fn test_check_success_post_when_failure() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();
    let config = GitCheckConfiguration::new();

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins).post_when(PostWhen::Failure);

    let result = check
        .check_mr(
            "test_check_success_post_when_failure",
            &CommitId::new(BASE),
            &mr,
        )
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    assert_eq!(service.remaining_data(), 0);
}

// A failed check should comment, and post a failure status on the branch and each failing commit;
// passing commits should still pass.
#[test]
fn test_check_failure() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();

    // Add a check which rejects the first commit on the branch.
    let bad_commit = git_checks::BadCommit::builder()
        .commit(CommitId::new(COMMIT))
        .reason("for testing purposes")
        .build()
        .unwrap();

    let mut config = GitCheckConfiguration::new();
    config.add_topic_check(&bad_commit);

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let result = check
        .check_mr("test_check_failure", &CommitId::new(BASE), &mr)
        .unwrap();
    assert_eq!(result, CheckStatus::Fail);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses_head = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        concat!(
            "Errors:\n\n",
            "  - commit 7189cf557ba2c7c61881ff8669158710b94d8df1 is not allowed for testing \
             purposes.\n",
            "\n",
            "Alerts:\n\n",
            "  - commit 7189cf557ba2c7c61881ff8669158710b94d8df1 was pushed to the server.\n",
            "\n",
            "Please rewrite commits to fix the errors listed above (adding fixup commits will not \
             resolve the errors) and force-push the branch again to update the merge request.\n",
            "\n",
            "Alert: @admin @admin2."
        ),
    );

    assert_eq!(mr_commit_statuses_head.len(), 2);

    let status = &mr_commit_statuses_head[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_commit_statuses_head[1];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);
}

// A failed check, but it is allowed via other means.
#[test]
fn test_check_failure_but_allowed() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();

    // Add a check which rejects the first commit on the branch.
    let bad_commit = git_checks::BadCommit::builder()
        .commit(CommitId::new(COMMIT))
        .reason("for testing purposes")
        .build()
        .unwrap();

    // Add a check which allows all failures.
    let allow = test_checks::Allow;

    let mut config = GitCheckConfiguration::new();
    config.add_topic_check(&bad_commit);
    config.add_topic_check(&allow);

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let result = check
        .check_mr("test_check_failure_but_allowed", &CommitId::new(BASE), &mr)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_commit_statuses_head = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses_head.len(), 2);

    let status = &mr_commit_statuses_head[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_commit_statuses_head[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);
}

// A failed check should comment, and post a failure status on the branch and each failing commit;
// passing commits should be ignored.
#[test]
fn test_check_failure_post_when_failure() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();

    // Add a check which rejects the first commit on the branch.
    let bad_commit = git_checks::BadCommit::builder()
        .commit(CommitId::new(COMMIT))
        .reason("for testing purposes")
        .build()
        .unwrap();

    let mut config = GitCheckConfiguration::new();
    config.add_topic_check(&bad_commit);

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins).post_when(PostWhen::Failure);

    let result = check
        .check_mr(
            "test_check_failure_post_when_failure",
            &CommitId::new(BASE),
            &mr,
        )
        .unwrap();
    assert_eq!(result, CheckStatus::Fail);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses_head = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        concat!(
            "Errors:\n\n",
            "  - commit 7189cf557ba2c7c61881ff8669158710b94d8df1 is not allowed for testing \
             purposes.\n",
            "\n",
            "Alerts:\n\n",
            "  - commit 7189cf557ba2c7c61881ff8669158710b94d8df1 was pushed to the server.\n",
            "\n",
            "Please rewrite commits to fix the errors listed above (adding fixup commits will not \
             resolve the errors) and force-push the branch again to update the merge request.\n",
            "\n",
            "Alert: @admin @admin2.",
        ),
    );

    assert_eq!(mr_commit_statuses_head.len(), 1);

    let status = &mr_commit_statuses_head[0];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);
}

// Checks should checkout backported commits properly.
#[test]
fn test_check_backport_commit() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();

    let config = GitCheckConfiguration::new();

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let result = check
        .check_mr_with(
            "test_check_backport_commit",
            &CommitId::new(BASE),
            &mr,
            &CommitId::new(COMMIT),
        )
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_update_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_update_commit_statuses.len(), 2);

    let status = &mr_update_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_update_commit_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1",
    );
    assert_eq!(status.target_url, None);
}

// Checks should bail out if a commit was requested to be checked that was not part of the merge
// request.
#[test]
fn test_check_unrelated_commit() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();

    let config = GitCheckConfiguration::new();

    let mr = service
        .merge_request(REPO_NAME, MR_MISSED_UPDATE_ID)
        .unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let err = check
        .check_mr_with(
            "test_check_unrelated_commit",
            &CommitId::new(BASE),
            &mr,
            &CommitId::new(COMMIT),
        )
        .unwrap_err();

    if let CheckError::UnrelatedCommit {
        commit,
    } = err
    {
        assert_eq!(commit, CommitId::new(COMMIT));
    } else {
        panic!("unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);
}

// Checks should bail out if a commit was requested to be checked that was already merged.
#[test]
fn test_check_already_merged() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();

    let config = GitCheckConfiguration::new();

    let mr = service
        .merge_request(REPO_NAME, MR_ALREADY_MERGED_ID)
        .unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let result = check
        .check_mr_with(
            "test_check_already_merged",
            &CommitId::new(BASE),
            &mr,
            &CommitId::new(COMMIT),
        )
        .unwrap();
    assert_eq!(result, CheckStatus::Fail);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses_head = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        concat!(
            "Errors:\n\n",
            "  - the merge request is already merged into 58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd.\n",
            "\n",
            "Please rewrite commits to fix the errors listed above (adding fixup commits will not \
             resolve the errors) and force-push the branch again to update the merge request.",
        ),
    );

    assert_eq!(mr_commit_statuses_head.len(), 2);

    let status = &mr_commit_statuses_head[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_commit_statuses_head[1];
    assert_eq!(status.state, CommitStatusState::Failed);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1",
    );
    assert_eq!(status.target_url, None);
}

// Checks should run branch checks exactly once.
#[test]
fn test_check_branch_checks_once() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();

    let always_warn = test_checks::AlwaysWarn;

    let mut config = GitCheckConfiguration::new();
    config.add_check(&always_warn);
    config.add_branch_check(&always_warn);

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let result = check
        .check_mr("test_check_branch_checks_once", &CommitId::new(BASE), &mr)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_update_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        concat!(
            "Warnings:\n\n",
            "  - This warning shows up for commit 7189cf557ba2c7c61881ff8669158710b94d8df1.\n",
            "  - This warning shows up for commit fe70f127605efb6032cacea0bd336428d67ed5a3.\n",
            "  - This warning shows up for every branch.\n",
            "\n",
            "The warnings do not need to be fixed, but it is recommended to do so."
        ),
    );

    assert_eq!(mr_commit_update_statuses.len(), 2);

    let status = &mr_commit_update_statuses[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_commit_update_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: fe70f127605efb6032cacea0bd336428d67ed5a3",
    );
    assert_eq!(status.target_url, None);
}

// A successful check with warnings should comment, and post a success status on the branch and
// each commit.
#[test]
fn test_check_warning() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();
    let mr = service.merge_request(REPO_NAME, MR_WIP_ID).unwrap();
    let config = GitCheckConfiguration::new();
    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let result = check
        .check_mr("test_check_warning", &CommitId::new(BASE), &mr)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        concat!(
            "Warnings:\n\n",
            "  - the merge request is marked as a work-in-progress.\n",
            "\n",
            "The warnings do not need to be fixed, but it is recommended to do so.",
        ),
    );

    assert_eq!(mr_commit_statuses.len(), 2);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: f6f8de8c7c5f1a081b14f5a47c7798268f383222",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_commit_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: f6f8de8c7c5f1a081b14f5a47c7798268f383222",
    );
    assert_eq!(status.target_url, None);
}

// A successful check with temporary warnings should comment, and post a success status on the
// branch and each commit.
#[test]
fn test_check_warning_temporary() {
    let tempdir = test_workspace_dir();
    let ctx = git_context(tempdir.path());
    let service = check_service();
    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();

    let temporary = test_checks::Temporary;

    let mut config = GitCheckConfiguration::new();
    config.add_check(&temporary);

    let admins = admins();
    let check = Check::new(ctx, service.clone(), config, &admins);

    let result = check
        .check_mr("test_check_warning_temporary", &CommitId::new(BASE), &mr)
        .unwrap();
    assert_eq!(result, CheckStatus::Pass);

    let mr_comments = service.mr_comments(mr.id);
    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        concat!(
            "Warnings:\n\n",
            "  - A temporary warning for commit 7189cf557ba2c7c61881ff8669158710b94d8df1.\n",
            "\n",
            "The warnings do not need to be fixed, but it is recommended to do so.\n",
            "\n",
            "Some messages may be temporary; please trigger the checks again if they have been \
             resolved.",
        ),
    );

    assert_eq!(mr_commit_statuses.len(), 2);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Pending);
    assert_eq!(status.refname, Some(mr.source_branch.clone()));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1",
    );
    assert_eq!(status.target_url, None);

    let status = &mr_commit_statuses[1];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(
        status.name,
        "ghostflow-check-58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd",
    );
    assert_eq!(
        status.description,
        "overall branch status for the content checks against \
         58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1",
    );
    assert_eq!(status.target_url, None);
}
