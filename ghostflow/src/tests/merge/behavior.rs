// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::iter;
use std::sync::Arc;

use crate::actions::merge::*;
use crate::host::HostingService;
use crate::tests::merge::utils;
use crate::tests::mock::{MockData, MockMergeRequest, MockService};
use crate::tests::utils::test_workspace_dir;

const REPO_NAME: &str = "base";
const MR_ID: u64 = 1;
const MR_UPDATE_ID: u64 = 2;
const MR_WIP_ID: u64 = 3;
const MR_ALREADY_MERGED_ID: u64 = 4;
const MR_UNRELATED_ID: u64 = 5;
const MR_CONFLICT_ID: u64 = 6;
const MR_REJECTED_BY_ID: u64 = 7;
const MR_MESSAGE_ID: u64 = 8;
const COMMIT: &str = "7189cf557ba2c7c61881ff8669158710b94d8df1";
const COMMIT_UPDATE: &str = "fe70f127605efb6032cacea0bd336428d67ed5a3";
const COMMIT_ALREADY_MERGED: &str = "6d60de8086a5eaa0350a4a9d6c1897cfdc49f479";
const COMMIT_UNRELATED: &str = "1da7f0e3f3f5f6948835aa4f77a2ee2c2ae43854";
const COMMIT_CONFLICT: &str = "7a28f8ea8759ff4f125ddbca825f976927a61310";

const BRANCH_NAME: &str = "test-merge";
const INTO_BRANCH_NAME: &str = "test-merge-into";
const INTO_BRANCH_NAME2: &str = "test-merge-into2";
const BASE: &str = "58b2dee73ab6e6b1f3587b41d0ccdbe2ded785dd";
const BASE_UPDATE: &str = "11bfbf44147015650afe6f95508ecfb1a77443cd";

fn merge_service() -> Arc<MockService> {
    let base = MockData::repo(REPO_NAME);
    let fork = MockData::forked_repo("fork", &base);
    let user = MockData::user("user");
    let maint = MockData::user("maint");
    MockData::builder()
        .add_merge_request(MockMergeRequest::new(MR_ID, &user, COMMIT, &base, &fork))
        .add_merge_request(
            MockMergeRequest::new(MR_UPDATE_ID, &user, COMMIT_UPDATE, &base, &fork)
                .old_commit(COMMIT),
        )
        .add_merge_request(
            MockMergeRequest::new(MR_WIP_ID, &user, COMMIT, &base, &fork).work_in_progress(),
        )
        .add_merge_request(MockMergeRequest::new(
            MR_ALREADY_MERGED_ID,
            &user,
            COMMIT_ALREADY_MERGED,
            &base,
            &fork,
        ))
        .add_merge_request(MockMergeRequest::new(
            MR_UNRELATED_ID,
            &user,
            COMMIT_UNRELATED,
            &base,
            &fork,
        ))
        .add_merge_request(MockMergeRequest::new(
            MR_CONFLICT_ID,
            &user,
            COMMIT_CONFLICT,
            &base,
            &fork,
        ))
        .add_merge_request(
            MockMergeRequest::new(MR_REJECTED_BY_ID, &user, COMMIT, &base, &fork)
                .add_comment(MockData::comment(&maint, "Rejected-by: me")),
        )
        .add_merge_request(
            MockMergeRequest::new(MR_MESSAGE_ID, &user, COMMIT, &base, &fork)
                .description("```message\nExtra message for merge commit\n```\n"),
        )
        .add_project(base)
        .add_project(fork)
        .add_user(maint)
        .add_user(user)
        .service()
}

// Merging should succeed.
#[test]
fn test_merge_simple() {
    let service = merge_service();
    let message = "Merge topic 'topic-1' into test-merge\n\
                   \n\
                   7189cf5 topic-1: make a change\n\
                   \n\
                   Acked-by: Ghostflow <ghostflow@example.com>\n\
                   Merge-request: !1\n";
    utils::simple_merge_behavior(service, MR_ID, message)
}

// Merging should succeed with the fast forward topology.
#[test]
fn test_merge_simple_ff() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let settings = {
        let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
        settings.merge_topology(MergeTopology::FastForwardOnly);
        settings
    };
    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit_ff(&origin_ctx, BRANCH_NAME, &mr.commit.id);
}

// Merging should succeed with the fast forward topology as a merge if allowed.
#[test]
fn test_merge_simple_ff_but_merges() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE_UPDATE);
    let service = merge_service();
    let settings = {
        let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
        settings.merge_topology(MergeTopology::FastForwardIfPossible);
        settings
    };
    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// Merging should fail with the fast forward topology if the base has moved on.
#[test]
fn test_merge_simple_ff_base_updated() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE_UPDATE);
    let service = merge_service();
    let settings = {
        let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());
        settings.merge_topology(MergeTopology::FastForwardOnly);
        settings
    };
    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request may not be merged into `test-merge` because a fast-forward merge was \
         requested, but is not possible because the branch is no longer an ancestor.",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: update base commit\n");
}

// Merging should succeed with a different branch name.
#[test]
fn test_merge_simple_branch_name() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge = utils::create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr_named(
            &mr,
            "topic-1-renamed",
            utils::merger_ident(),
            utils::author_date(),
        )
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1-renamed' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// Merging should succeed with the right "into" branch name.
#[test]
fn test_merge_simple_as_named() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.merge_branch_as(Some("custom-branch"));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into custom-branch\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// Merging should succeed without the "into" name for elided branches.
#[test]
fn test_merge_simple_as_master() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.merge_branch_as(Some("master"));
    settings.elide_branch_name(true);

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1'\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// Merging should not comment when quiet.
#[test]
fn test_merge_simple_quiet() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.quiet();

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    assert_eq!(service.remaining_data(), 0);

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// Merging should create a log without elision if the limit is over the length.
#[test]
fn test_merge_simple_log_limit_over() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.log_limit(Some(2));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// Merging should not elide if the limit is exactly the topic length.
#[test]
fn test_merge_simple_log_limit_exact() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.log_limit(Some(1));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// Merging should elide if the topic is longer than the limit.
#[test]
fn test_merge_simple_log_limit_elide() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.log_limit(Some(1));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_UPDATE_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-2' into test-merge\n\
         \n\
         fe70f12 topic-1: update\n\
         ...\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !2\n",
    );
}

// Merging should not log if the limit is zero.
#[test]
fn test_merge_simple_log_limit_elide_zero() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.log_limit(Some(0));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
}

// Merging WIP branches should fail.
#[test]
fn test_merge_wip() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge = utils::create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request(REPO_NAME, MR_WIP_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request is marked as a Work in Progress and may not be merged. Please remove \
         the Work in Progress state first.",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
}

// Test failure to push to the remote server.
#[test]
fn test_merge_push_fail() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge = utils::create_merge(&ctx, BRANCH_NAME, &service);

    // Make a change on remote the clone is not aware of.
    utils::make_commit(&origin_ctx, BRANCH_NAME);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::PushFailed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "Automatic merge succeeded, but pushing to the remote failed.",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "blocking commit\n");
}

// Test failure to push to the remote server.
#[test]
fn test_merge_push_fail_quiet() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.quiet();

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    // Make a change on remote the clone is not aware of.
    utils::make_commit(&origin_ctx, BRANCH_NAME);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::PushFailed);

    assert_eq!(service.remaining_data(), 0);

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "blocking commit\n");
}

// Already merged branches should be ignored.
#[test]
fn test_merge_already_merged() {
    let tempdir = test_workspace_dir();
    let (_, ctx) = utils::git_context(tempdir.path(), BASE_UPDATE);
    let service = merge_service();
    let merge = utils::create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service
        .merge_request(REPO_NAME, MR_ALREADY_MERGED_ID)
        .unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request may not be merged into `test-merge` because it has already been \
         merged.",
    );
}

// Branches which do not share history should not be allowed.
#[test]
fn test_merge_no_common_history() {
    let tempdir = test_workspace_dir();
    let (_, ctx) = utils::git_context(tempdir.path(), BASE_UPDATE);
    let service = merge_service();
    let merge = utils::create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request(REPO_NAME, MR_UNRELATED_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request may not be merged into `test-merge` because there is no common \
         history.",
    );
}

// Conflicts should be reported.
#[test]
fn test_merge_conflict() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE_UPDATE);
    let service = merge_service();
    let merge = utils::create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request(REPO_NAME, MR_CONFLICT_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request contains conflicts with `test-merge` in the following paths:\n\n  - \
         `base`",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: update base commit\n");
}

// Policies should be allowed to reject merge requests.
#[test]
fn test_merge_policy_rejection() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge = utils::create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request(REPO_NAME, MR_REJECTED_BY_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Failed);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(
        mr_comments[0],
        "This merge request may not be merged into `test-merge` because:\n\n  - rejected by \
         @maint",
    );

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
}

// base -> into
#[test]
fn test_merge_simple_into() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.add_into_branches(iter::once(IntoBranch::new(INTO_BRANCH_NAME)));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME,
        "Merge branch 'test-merge' into test-merge-into",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
}

// base -> into
#[test]
fn test_merge_simple_into_ff() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.add_into_branches(iter::once(IntoBranch::new(INTO_BRANCH_NAME)));
    settings.merge_topology(MergeTopology::FastForwardOnly);

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit_ff(&origin_ctx, BRANCH_NAME, &mr.commit.id);
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME,
        "Merge branch 'test-merge' into test-merge-into",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
}

// base -> into
#[test]
fn test_merge_simple_into_with_name() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.add_into_branches(iter::once(IntoBranch::new(INTO_BRANCH_NAME)));
    settings.merge_branch_as(Some("custom-branch"));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into custom-branch\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME,
        "Merge branch 'custom-branch' into test-merge-into",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
}

// base -> base
#[test]
fn test_merge_simple_into_cycle() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.add_into_branches(iter::once(IntoBranch::new(BRANCH_NAME)));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let err = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap_err();

    if let MergeError::CircularIntoBranches {} = err {
        // OK
    } else {
        panic!("unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);

    utils::check_mr_commit_message(&origin_ctx, BRANCH_NAME, "base: add a base commit\n");
}

// base -> into
//      -> into2
#[test]
fn test_merge_simple_into_many() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    settings.add_into_branches(vec![
        IntoBranch::new(INTO_BRANCH_NAME),
        IntoBranch::new(INTO_BRANCH_NAME2),
    ]);

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME,
        "Merge branch 'test-merge' into test-merge-into",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME2,
        "Merge branch 'test-merge' into test-merge-into2",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME2);
}

// base -> into -> into2
#[test]
fn test_merge_simple_into_chain() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    let mut into_branch = IntoBranch::new(INTO_BRANCH_NAME);
    into_branch.chain_into(iter::once(IntoBranch::new(INTO_BRANCH_NAME2)));
    settings.add_into_branches(iter::once(into_branch));

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME,
        "Merge branch 'test-merge' into test-merge-into",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME2,
        "Merge branch 'test-merge-into' into test-merge-into2",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME2);
}

// base -> into -> into2
//      -> into2
#[test]
fn test_merge_simple_into_multi() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let mut settings = MergeSettings::new(BRANCH_NAME, utils::TestMergePolicy::default());

    let mut into_branch = IntoBranch::new(INTO_BRANCH_NAME);
    let into_branch2 = IntoBranch::new(INTO_BRANCH_NAME2);
    into_branch.chain_into(iter::once(into_branch2.clone()));
    settings.add_into_branches(vec![into_branch, into_branch2]);

    let merge = utils::create_merge_settings(&ctx, &service, settings);

    let mr = service.merge_request(REPO_NAME, MR_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-1' into test-merge\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !1\n",
    );
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME,
        "Merge branch 'test-merge' into test-merge-into",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME);
    utils::check_mr_commit(
        &origin_ctx,
        INTO_BRANCH_NAME2,
        "Merge branch 'test-merge-into' into test-merge-into2",
    );
    utils::check_noop_merge(&origin_ctx, INTO_BRANCH_NAME2);
    let parent = format!("{}~", INTO_BRANCH_NAME2);
    utils::check_mr_commit(
        &origin_ctx,
        &parent,
        "Merge branch 'test-merge' into test-merge-into2",
    );
    utils::check_noop_merge(&origin_ctx, &parent);
}

#[test]
fn test_merge_commit_message_from_description() {
    let tempdir = test_workspace_dir();
    let (origin_ctx, ctx) = utils::git_context(tempdir.path(), BASE);
    let service = merge_service();
    let merge = utils::create_merge(&ctx, BRANCH_NAME, &service);

    let mr = service.merge_request(REPO_NAME, MR_MESSAGE_ID).unwrap();
    let res = merge
        .merge_mr(&mr, utils::merger_ident(), utils::author_date())
        .unwrap();
    assert_eq!(res, MergeActionResult::Success);

    let mr_comments = service.mr_comments(mr.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_comments.len(), 1);
    assert_eq!(mr_comments[0], "Topic successfully merged and pushed.");

    utils::check_mr_commit(
        &origin_ctx,
        BRANCH_NAME,
        "Merge topic 'topic-8' into test-merge\n\
         \n\
         Extra message for merge commit\n\
         \n\
         7189cf5 topic-1: make a change\n\
         \n\
         Acked-by: Ghostflow <ghostflow@example.com>\n\
         Merge-request: !8\n",
    );
}
