// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::Arc;

use regex::Regex;

use crate::actions::test::pipelines::*;
use crate::host::{HostedPipelineService, HostingService, PipelineState};
use crate::tests::mock::{MockData, MockMergeRequest, MockPipeline, MockService};

const REPO_NAME: &str = "base";
const MR_FULL_ID: u64 = 10;
const MR_EMPTY_ID: u64 = 11;
const MR_DISABLED_CI_ID: u64 = 12;
const MR_NO_PIPELINES_ID: u64 = 13;
const COMMIT_FULL: &str = "7189cf557ba2c7c61881ff8669158710b94d8df1";
const COMMIT_EMPTY: &str = "755842266dcc5739c06d61433241f44b9306f24c";
const COMMIT_NO_PIPELINES: &str = "1b340d2edcf19077ab3e27ddda7430a6612c2f62";
const JOB_MANUAL_ID: u64 = 0;
const JOB_IN_PROGRESS_ID: u64 = 1;
const JOB_CANCELED_ID: u64 = 2;
const JOB_FAILED_ID: u64 = 3;
const JOB_SUCCESS_ID: u64 = 4;

// Test the manual `impl Debug`
#[test]
fn test_debug_impl() {
    let service = MockData::builder().service();
    let pipeline_service = service.as_pipeline_service().unwrap();
    let test = TestPipelines::new(pipeline_service);

    assert_eq!(format!("{:?}", test), "TestPipelines");
}

// Test for an MR with no CI enabled.
#[test]
fn test_ci_disabled() {
    let base = MockData::repo(REPO_NAME);
    let fork = MockData::forked_repo("fork", &base);
    let user = MockData::user("user");
    let service = MockData::builder()
        .add_merge_request(MockMergeRequest::new(
            MR_DISABLED_CI_ID,
            &user,
            COMMIT_NO_PIPELINES,
            &base,
            &fork,
        ))
        .add_project(base)
        .add_project(fork)
        .add_user(user)
        .service();
    let pipeline_service = service.clone().as_pipeline_service().unwrap();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_DISABLED_CI_ID).unwrap();

    let options = TestPipelinesOptions::builder().build().unwrap();
    let err = test.test_mr(&mr, &options).unwrap_err();
    if let TestPipelinesError::NoPipelinesAvailable = err {
        // Expected error
    } else {
        panic!("Unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);
}

// Test for an MR with no pipelines for the commit.
#[test]
fn test_no_pipelines() {
    let base = MockData::repo(REPO_NAME);
    let user = MockData::user("user");
    let service = MockData::builder()
        .add_merge_request(MockMergeRequest::new(
            MR_NO_PIPELINES_ID,
            &user,
            COMMIT_NO_PIPELINES,
            &base,
            &base,
        ))
        .add_project(base)
        .add_user(user)
        .service();
    let pipeline_service = service.clone().as_pipeline_service().unwrap();
    let test = TestPipelines::new(pipeline_service);

    let mr = service
        .merge_request(REPO_NAME, MR_NO_PIPELINES_ID)
        .unwrap();

    let options = TestPipelinesOptions::builder().build().unwrap();
    let err = test.test_mr(&mr, &options).unwrap_err();
    if let TestPipelinesError::NoPipelines = err {
        // Expected error
    } else {
        panic!("Unexpected error: {:?}", err);
    }

    assert_eq!(service.remaining_data(), 0);
}

// Test for an MR with a job-less pipeline enabled.
#[test]
fn test_empty_pipelines() {
    let base = MockData::repo(REPO_NAME);
    let user = MockData::user("user");
    let service = MockData::builder()
        .add_merge_request(MockMergeRequest::new(
            MR_EMPTY_ID,
            &user,
            COMMIT_EMPTY,
            &base,
            &base,
        ))
        .add_pipeline(MockPipeline::new(0, &base, COMMIT_EMPTY))
        .add_project(base)
        .add_user(user)
        .service();
    let pipeline_service = service.clone().as_pipeline_service().unwrap();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_EMPTY_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::RestartAll)
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.remaining_data(), 0);
}

fn test_pipelines_service() -> (Arc<MockService>, Arc<dyn HostedPipelineService>) {
    let base = MockData::repo(REPO_NAME);
    let user = MockData::user("user");
    let mut builder = MockData::builder();
    builder
        .add_merge_request(MockMergeRequest::new(
            MR_FULL_ID,
            &user,
            COMMIT_FULL,
            &base,
            &base,
        ))
        .add_user(user);

    let jobs = [
        (JOB_MANUAL_ID, PipelineState::Manual, "manual"),
        (JOB_IN_PROGRESS_ID, PipelineState::InProgress, "in_progress"),
        (JOB_CANCELED_ID, PipelineState::Canceled, "canceled"),
        (JOB_FAILED_ID, PipelineState::Failed, "failed"),
        (JOB_SUCCESS_ID, PipelineState::Success, "success"),
    ];
    let mut pipeline = MockPipeline::new(0, &base, COMMIT_FULL);
    for (job_id, state, name) in &jobs {
        pipeline.add_job(*job_id);
        builder.add_job(MockData::job(&base, *state, name, *job_id));
    }
    builder.add_pipeline(pipeline).add_project(base);

    let service = builder.service();
    let pipeline_service = service.clone().as_pipeline_service().unwrap();
    (service, pipeline_service)
}

// Test each action on a full pipeline.
#[test]
fn test_default_action() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder().build().unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_MANUAL_ID), &[None]);

    assert_eq!(service.remaining_data(), 0);
}

#[test]
fn test_start_manual_action() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::StartManual)
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_MANUAL_ID), &[None]);

    assert_eq!(service.remaining_data(), 0);
}

#[test]
fn test_restart_unsuccessful_action() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::RestartUnsuccessful)
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_CANCELED_ID), &[None]);
    assert_eq!(service.jobs(JOB_FAILED_ID), &[None]);

    assert_eq!(service.remaining_data(), 0);
}

#[test]
fn test_restart_failed_action() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::RestartFailed)
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_FAILED_ID), &[None]);

    assert_eq!(service.remaining_data(), 0);
}

#[test]
fn test_restart_all_action() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::RestartAll)
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_CANCELED_ID), &[None]);
    assert_eq!(service.jobs(JOB_FAILED_ID), &[None]);
    assert_eq!(service.jobs(JOB_SUCCESS_ID), &[None]);

    assert_eq!(service.remaining_data(), 0);
}

// Test stage matching.
#[test]
fn test_stage_matching() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::RestartUnsuccessful)
        .stage("failed")
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_FAILED_ID), &[None]);

    assert_eq!(service.remaining_data(), 0);
}

// Test job matching.
#[test]
fn test_job_matching() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::RestartAll)
        // Should match "canceled" and "success"
        .jobs_matching(Regex::new("c.").unwrap())
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_CANCELED_ID), &[None]);
    assert_eq!(service.jobs(JOB_SUCCESS_ID), &[None]);

    assert_eq!(service.remaining_data(), 0);
}

// Test any regex job matching.
#[test]
fn test_job_matching_any() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::RestartAll)
        // Should match "canceled" and "success"
        .jobs_matching_any_of(
            [Regex::new("c.n").unwrap(), Regex::new("s.c").unwrap()]
                .iter()
                .cloned(),
        )
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_CANCELED_ID), &[None]);
    assert_eq!(service.jobs(JOB_SUCCESS_ID), &[None]);

    assert_eq!(service.remaining_data(), 0);
}

// Test user masquerading.
#[test]
fn test_user_masquerade() {
    let (service, pipeline_service) = test_pipelines_service();
    let test = TestPipelines::new(pipeline_service);

    let mr = service.merge_request(REPO_NAME, MR_FULL_ID).unwrap();

    let options = TestPipelinesOptions::builder()
        .action(TestPipelinesAction::RestartAll)
        .user("user")
        .build()
        .unwrap();
    test.test_mr(&mr, &options).unwrap();

    assert_eq!(service.jobs(JOB_CANCELED_ID), &[Some("user".into())]);
    assert_eq!(service.jobs(JOB_FAILED_ID), &[Some("user".into())]);
    assert_eq!(service.jobs(JOB_SUCCESS_ID), &[Some("user".into())]);

    assert_eq!(service.remaining_data(), 0);
}
