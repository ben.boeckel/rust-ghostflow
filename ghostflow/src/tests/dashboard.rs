// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::Arc;

use git_workarea::CommitId;

use crate::actions::dashboard::*;
use crate::host::{CommitStatusState, HostingService};
use crate::tests::mock::{MockData, MockMergeRequest, MockService};

const REPO_NAME: &str = "base";
const MR_WITHOUT_PIPELINE: u64 = 8;
const MR_WITH_PIPELINE: u64 = 9;
const PIPELINE_ID: u64 = 1;
const STATUS: &str = "dashboard-simple";
const URL: &str = "https://example.com/dashboard/for/{commit}";
const COMMIT: &str = "7189cf557ba2c7c61881ff8669158710b94d8df1";
const ALTERED_COMMIT: &str = "f6f8de8c7c5f1a081b14f5a47c7798268f383222";
const MR_DESCRIPTION: &str = "Dashboard results for the MR.\n\
                              \n\
                              Branch-at: {commit}\n\
                              Source-branch: {source_branch}\n\
                              Target-branch: {target_branch}\n\
                              Merge-request: !{mr_id}\n\
                              Unknown-expansion: {unknown}\n\
                              Pipeline-id: {pipeline_id}";
const COMMIT_DESCRIPTION: &str = "Dashboard results for the commit.\n\
                                  \n\
                                  Branch-at: {commit}\n\
                                  Refname: {refname}\n\
                                  Branch-name: {branch_name}\n\
                                  Tag-name: {tag_name}\n\
                                  Unknown-expansion: {unknown}\n\
                                  Pipeline-id: {pipeline_id}";

fn dashboard_service() -> Arc<MockService> {
    let base = MockData::repo(REPO_NAME);
    let user = MockData::user("user");
    MockData::builder()
        .add_merge_request(MockMergeRequest::new(
            MR_WITHOUT_PIPELINE,
            &user,
            COMMIT,
            &base,
            None,
        ))
        .add_merge_request(
            MockMergeRequest::new(MR_WITH_PIPELINE, &user, COMMIT, &base, None)
                .with_pipeline(PIPELINE_ID),
        )
        .add_project(base)
        .add_user(user)
        .service()
}

// Check that statuses are sent properly for MRs.
#[test]
fn test_dashboard_simple_mr() {
    let service = dashboard_service();
    let mr = service.merge_request(REPO_NAME, MR_WITH_PIPELINE).unwrap();

    let dashboard = Dashboard::new(service.clone(), STATUS, URL, MR_DESCRIPTION);

    dashboard.post_for_mr(&mr).unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(status.name, "dashboard-simple");
    assert_eq!(
        status.target_url,
        Some("https://example.com/dashboard/for/7189cf557ba2c7c61881ff8669158710b94d8df1".into()),
    );
    assert_eq!(
        status.description,
        "Dashboard results for the MR.\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1\n\
         Source-branch: topic-9\n\
         Target-branch: master\n\
         Merge-request: !9\n\
         Unknown-expansion: \n\
         Pipeline-id: 1",
    );
}

// Check that statuses are sent properly for commits.
#[test]
fn test_dashboard_simple_commit() {
    let service = dashboard_service();
    let mr = service.merge_request(REPO_NAME, MR_WITH_PIPELINE).unwrap();

    let dashboard = Dashboard::new(service.clone(), STATUS, URL, COMMIT_DESCRIPTION);

    dashboard.post_for_commit(&mr.commit).unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(status.name, "dashboard-simple");
    assert_eq!(
        status.target_url,
        Some("https://example.com/dashboard/for/7189cf557ba2c7c61881ff8669158710b94d8df1".into()),
    );
    assert_eq!(
        status.description,
        "Dashboard results for the commit.\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1\n\
         Refname: topic-9\n\
         Branch-name: \n\
         Tag-name: \n\
         Unknown-expansion: \n\
         Pipeline-id: 1",
    );
}

// Check that statuses are sent properly for MRs without a pipeline id.
#[test]
fn test_dashboard_simple_mr_no_pipeline_id() {
    let service = dashboard_service();
    let mr = service
        .merge_request(REPO_NAME, MR_WITHOUT_PIPELINE)
        .unwrap();

    let dashboard = Dashboard::new(service.clone(), STATUS, URL, MR_DESCRIPTION);

    dashboard.post_for_mr(&mr).unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(status.name, "dashboard-simple");
    assert_eq!(
        status.target_url,
        Some("https://example.com/dashboard/for/7189cf557ba2c7c61881ff8669158710b94d8df1".into()),
    );
    assert_eq!(
        status.description,
        "Dashboard results for the MR.\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1\n\
         Source-branch: topic-8\n\
         Target-branch: master\n\
         Merge-request: !8\n\
         Unknown-expansion: \n\
         Pipeline-id: ",
    );
}

// Check that statuses are sent properly for commits without a pipeline id.
#[test]
fn test_dashboard_simple_commit_no_pipeline_id() {
    let service = dashboard_service();
    let mr = service
        .merge_request(REPO_NAME, MR_WITHOUT_PIPELINE)
        .unwrap();

    let dashboard = Dashboard::new(service.clone(), STATUS, URL, COMMIT_DESCRIPTION);

    dashboard.post_for_commit(&mr.commit).unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(status.name, "dashboard-simple");
    assert_eq!(
        status.target_url,
        Some("https://example.com/dashboard/for/7189cf557ba2c7c61881ff8669158710b94d8df1".into()),
    );
    assert_eq!(
        status.description,
        "Dashboard results for the commit.\n\
         \n\
         Branch-at: 7189cf557ba2c7c61881ff8669158710b94d8df1\n\
         Refname: topic-8\n\
         Branch-name: \n\
         Tag-name: \n\
         Unknown-expansion: \n\
         Pipeline-id: ",
    );
}

// Check that statuses are sent properly for MRs with "altered" information.
#[test]
fn test_dashboard_altered_mr() {
    let service = dashboard_service();
    let mr = service.merge_request(REPO_NAME, MR_WITH_PIPELINE).unwrap();
    let mr_altered_commit = {
        let mut commit = mr.commit.clone();
        commit.id = CommitId::new(ALTERED_COMMIT);
        commit
    };

    let dashboard = Dashboard::new(service.clone(), STATUS, URL, MR_DESCRIPTION);

    dashboard
        .post_for_mr_altered(&mr, &mr_altered_commit)
        .unwrap();

    let mr_commit_statuses = service.commit_statuses(&mr.commit.id);

    assert_eq!(service.remaining_data(), 0);

    assert_eq!(mr_commit_statuses.len(), 1);

    let status = &mr_commit_statuses[0];
    assert_eq!(status.state, CommitStatusState::Success);
    assert_eq!(status.refname, Some(mr.source_branch));
    assert_eq!(status.name, "dashboard-simple");
    assert_eq!(
        status.target_url,
        Some("https://example.com/dashboard/for/f6f8de8c7c5f1a081b14f5a47c7798268f383222".into()),
    );
    assert_eq!(
        status.description,
        "Dashboard results for the MR.\n\
         \n\
         Branch-at: f6f8de8c7c5f1a081b14f5a47c7798268f383222\n\
         Source-branch: topic-9\n\
         Target-branch: master\n\
         Merge-request: !9\n\
         Unknown-expansion: \n\
         Pipeline-id: 1",
    );
}
