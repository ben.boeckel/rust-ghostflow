// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::collections::hash_map::HashMap;
use std::ops;
use std::sync::{Arc, Mutex};

use git_workarea::{CommitId, GitContext};
use itertools::Itertools;
use log::info;
use thiserror::Error;

use crate::host::*;
use crate::tests::mock::{MockData, MockDataBuilder, MockIssue, MockMergeRequest, MockPipeline};

const LOCK_POISONED: &str = "mock service lock poisoned";

#[derive(Default)]
struct State {
    issue_comments: HashMap<u64, Vec<String>>,
    issue_labels: HashMap<u64, Vec<String>>,
    mr_comments: HashMap<u64, Vec<String>>,
    commit_comments: HashMap<String, Vec<String>>,
    commit_statuses: HashMap<String, Vec<CommitStatus>>,
    jobs: HashMap<u64, Vec<Option<String>>>,
}

impl State {
    fn remaining(&self) -> usize {
        self.issue_comments.len()
            + self.issue_labels.len()
            + self.mr_comments.len()
            + self.commit_comments.len()
            + self.commit_statuses.len()
            + self.jobs.len()
    }
}

pub struct MockService {
    data: Mutex<MockData>,
    user: User,
    state: Mutex<State>,
}

impl MockService {
    pub(in crate::tests::mock) fn new(data: MockData, admin: User) -> Self {
        Self {
            data: Mutex::new(data),
            user: admin,
            state: Mutex::new(State::default()),
        }
    }

    pub fn modify(&self) -> MockDataModifier {
        let data = self.data.lock().expect(LOCK_POISONED);
        MockDataModifier {
            builder: MockDataBuilder::from_data(data.clone()),
            service: self,
        }
    }

    fn find_user(&self, user: &str) -> Result<User, MockHostError> {
        self.data
            .lock()
            .expect(LOCK_POISONED)
            .find_user(user)
            .cloned()
            .ok_or_else(|| MockHostError::invalid_user(user.into()))
    }

    fn find_project(&self, project: &str) -> Result<Repo, MockHostError> {
        self.data
            .lock()
            .expect(LOCK_POISONED)
            .find_project(project)
            .cloned()
            .ok_or_else(|| MockHostError::invalid_project(project.into()))
    }

    fn find_pipeline(&self, pipeline: u64) -> Result<MockPipeline, MockHostError> {
        self.data
            .lock()
            .expect(LOCK_POISONED)
            .find_pipeline(pipeline)
            .cloned()
            .ok_or_else(|| MockHostError::invalid_pipeline(pipeline))
    }

    fn find_job(&self, job: u64) -> Result<PipelineJob, MockHostError> {
        self.data
            .lock()
            .expect(LOCK_POISONED)
            .find_job(job)
            .cloned()
            .ok_or_else(|| MockHostError::invalid_job(job))
    }

    fn find_issue(&self, id: u64) -> Result<MockIssue, MockHostError> {
        self.data
            .lock()
            .expect(LOCK_POISONED)
            .find_issue(id)
            .cloned()
            .ok_or_else(|| MockHostError::invalid_issue(id))
    }

    fn find_merge_request(&self, id: u64) -> Result<MockMergeRequest, MockHostError> {
        self.data
            .lock()
            .expect(LOCK_POISONED)
            .find_merge_request(id)
            .cloned()
            .ok_or_else(|| MockHostError::invalid_merge_request(id))
    }

    pub fn mr_comments(&self, id: u64) -> Vec<String> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .mr_comments
            .remove(&id)
            .unwrap_or_default()
    }

    pub fn commit_statuses(&self, commit: &CommitId) -> Vec<CommitStatus> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .commit_statuses
            .remove(commit.as_str())
            .unwrap_or_default()
    }

    pub fn jobs(&self, job: u64) -> Vec<Option<String>> {
        self.state
            .lock()
            .expect(LOCK_POISONED)
            .jobs
            .remove(&job)
            .unwrap_or_default()
    }

    pub fn reset_state(&self) {
        let mut lock = self.state.lock().expect(LOCK_POISONED);
        *lock = State::default();
    }

    pub fn remaining_data(&self) -> usize {
        self.state.lock().expect(LOCK_POISONED).remaining()
    }
}

#[derive(Debug, Error)]
#[non_exhaustive]
enum MockHostError {
    #[error("invalid user: {}", user)]
    InvalidUser { user: String },
    #[error("invalid project: {}", project)]
    InvalidProject { project: String },
    #[error("invalid pipeline: {}", pipeline)]
    InvalidPipeline { pipeline: u64 },
    #[error("invalid job: {}", job)]
    InvalidJob { job: u64 },
    #[error("invalid issue: {}", issue)]
    InvalidIssue { issue: u64 },
    #[error("invalid merge request: {}", mr)]
    InvalidMergeRequest { mr: u64 },
    #[error("rejecting new labels")]
    RejectingLabels {},
}

impl MockHostError {
    fn invalid_user(user: String) -> Self {
        MockHostError::InvalidUser {
            user,
        }
    }

    fn invalid_project(project: String) -> Self {
        MockHostError::InvalidProject {
            project,
        }
    }

    fn invalid_pipeline(pipeline: u64) -> Self {
        MockHostError::InvalidPipeline {
            pipeline,
        }
    }

    fn invalid_job(job: u64) -> Self {
        MockHostError::InvalidJob {
            job,
        }
    }

    fn invalid_issue(issue: u64) -> Self {
        MockHostError::InvalidIssue {
            issue,
        }
    }

    fn invalid_merge_request(mr: u64) -> Self {
        MockHostError::InvalidMergeRequest {
            mr,
        }
    }

    fn rejecting_labels() -> Self {
        MockHostError::RejectingLabels {}
    }
}

impl From<MockHostError> for HostingServiceError {
    fn from(mock: MockHostError) -> Self {
        HostingServiceError::host(mock)
    }
}

fn commit_status_from_pending(status: &PendingCommitStatus, author: User) -> CommitStatus {
    CommitStatus {
        state: status.state,
        author,
        refname: status.commit.refname.clone(),
        name: status.name.into(),
        description: status.description.into(),
        target_url: status.target_url.map(Into::into),
    }
}

impl HostingService for MockService {
    fn service_user(&self) -> &User {
        &self.user
    }

    fn fetch_commit(&self, _: &GitContext, _: &Commit) -> Result<(), HostingServiceError> {
        Ok(())
    }

    fn fetch_mr(&self, _: &GitContext, _: &MergeRequest) -> Result<(), HostingServiceError> {
        Ok(())
    }

    fn as_pipeline_service(self: Arc<Self>) -> Option<Arc<dyn HostedPipelineService>> {
        Some(self as Arc<dyn HostedPipelineService>)
    }

    fn user(&self, project: &str, user: &str) -> Result<User, HostingServiceError> {
        let _ = self.find_project(project)?;

        Ok(self.find_user(user)?)
    }

    fn commit(&self, project: &str, commit: &CommitId) -> Result<Commit, HostingServiceError> {
        let project = self.find_project(project)?;

        Ok(Commit {
            repo: project,
            refname: None,
            // Just act as if the commit is a part of the project.
            id: commit.clone(),
            last_pipeline: None,
        })
    }

    fn merge_request(&self, project: &str, id: u64) -> Result<MergeRequest, HostingServiceError> {
        let _ = self.find_project(project)?;

        Ok(self.find_merge_request(id)?.mr().clone())
    }

    fn repo(&self, project: &str) -> Result<Repo, HostingServiceError> {
        Ok(self.find_project(project)?)
    }

    fn get_mr_comments(&self, mr: &MergeRequest) -> Result<Vec<Comment>, HostingServiceError> {
        let mr = self.find_merge_request(mr.id)?;

        Ok(mr
            .comments()
            .iter()
            .map(|mr_comment| mr_comment.0.clone())
            .collect())
    }

    fn post_mr_comment(&self, mr: &MergeRequest, content: &str) -> Result<(), HostingServiceError> {
        info!(
            target: "test/service",
            "posting a comment to MR#{}: {}",
            mr.id, content,
        );

        let mut state = self.state.lock().expect(LOCK_POISONED);
        let comments = state.mr_comments.entry(mr.id).or_insert_with(Vec::new);

        comments.push(content.into());

        Ok(())
    }

    fn get_commit_statuses(
        &self,
        commit: &Commit,
    ) -> Result<Vec<CommitStatus>, HostingServiceError> {
        Ok(self
            .state
            .lock()
            .expect(LOCK_POISONED)
            .commit_statuses
            .get(commit.id.as_str())
            .map_or_else(Vec::new, Clone::clone))
    }

    fn post_commit_status(&self, status: PendingCommitStatus) -> Result<(), HostingServiceError> {
        info!(
            target: "test/service",
            "posting a {:?} status to commit {} ({:?}) by {}: {}",
            status.state, status.commit.id, status.commit.refname,
            status.name, status.description,
        );

        let mut state = self.state.lock().expect(LOCK_POISONED);
        let statuses = state
            .commit_statuses
            .entry(status.commit.id.as_str().into())
            .or_insert_with(Vec::new);

        statuses.push(commit_status_from_pending(&status, self.user.clone()));

        Ok(())
    }

    fn get_mr_awards(&self, mr: &MergeRequest) -> Result<Vec<Award>, HostingServiceError> {
        let mr = self.find_merge_request(mr.id)?;

        Ok(mr.awards().clone())
    }

    fn issues_closed_by_mr(&self, mr: &MergeRequest) -> Result<Vec<Issue>, HostingServiceError> {
        let mr = self.find_merge_request(mr.id)?;

        mr.closes_issues()
            .iter()
            .map(|&id| Ok(self.find_issue(id).map(|issue| issue.issue().clone())?))
            .collect()
    }

    fn add_issue_labels(&self, issue: &Issue, labels: &[&str]) -> Result<(), HostingServiceError> {
        let mock_issue = self.find_issue(issue.id)?;
        if mock_issue.should_reject_labels() {
            return Err(MockHostError::rejecting_labels().into());
        }

        info!(
            target: "test/service",
            "adding labels to Issue#{}: `{}`",
            issue.id, labels.iter().join("`, `"),
        );

        let mut state = self.state.lock().expect(LOCK_POISONED);
        let cur_labels = state.issue_labels.entry(issue.id).or_insert_with(Vec::new);

        cur_labels.extend(labels.iter().map(|&label| label.into()));

        Ok(())
    }
}

impl HostedPipelineService for MockService {
    fn pipelines_for_mr(
        &self,
        mr: &MergeRequest,
    ) -> Result<Option<Vec<Pipeline>>, HostingServiceError> {
        // Forked projects have CI disabled.
        if mr
            .source_repo
            .as_ref()
            .map_or(true, |repo| repo.forked_from.is_some())
        {
            return Ok(None);
        }

        Ok(Some(
            self.data
                .lock()
                .expect(LOCK_POISONED)
                .pipelines()
                .filter_map(|pipeline| {
                    if pipeline.pipeline().commit.id == mr.commit.id {
                        Some(pipeline.pipeline().clone())
                    } else {
                        None
                    }
                })
                .collect(),
        ))
    }

    fn pipeline_jobs(
        &self,
        pipeline: &Pipeline,
    ) -> Result<Option<Vec<PipelineJob>>, HostingServiceError> {
        Ok(Some(
            self.find_pipeline(pipeline.id)?
                .jobs()
                .iter()
                .map(|&job| self.find_job(job))
                .collect::<Result<Vec<_>, _>>()?
                .into_iter()
                .collect(),
        ))
    }

    fn trigger_job(
        &self,
        job: &PipelineJob,
        user: Option<&str>,
    ) -> Result<(), HostingServiceError> {
        let _ = self.find_job(job.id)?;

        info!(
            target: "test/service",
            "triggering job {} via {:?}",
            job.id, user,
        );

        let mut state = self.state.lock().expect(LOCK_POISONED);
        state
            .jobs
            .entry(job.id)
            .or_insert_with(Vec::new)
            .push(user.map(Into::into));

        Ok(())
    }
}

pub struct MockDataModifier<'a> {
    builder: MockDataBuilder,
    service: &'a MockService,
}

impl<'a> MockDataModifier<'a> {
    pub fn apply(self) {
        let mut data = self.service.data.lock().expect(LOCK_POISONED);
        *data = self.builder.into_inner();
    }
}

impl<'a> ops::Deref for MockDataModifier<'a> {
    type Target = MockDataBuilder;

    fn deref(&self) -> &MockDataBuilder {
        &self.builder
    }
}

impl<'a> ops::DerefMut for MockDataModifier<'a> {
    fn deref_mut(&mut self) -> &mut MockDataBuilder {
        &mut self.builder
    }
}
