// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Git hosting service traits
//!
//! The traits in this module are meant to help keep service-specific knowledge away from the
//! workflow implementation.
//!
//! These traits might be split into a separate library in the future.

mod pipelines;
mod traits;
mod types;

pub use self::pipelines::HostedPipelineService;
pub use self::pipelines::Pipeline;
pub use self::pipelines::PipelineJob;
pub use self::pipelines::PipelineState;

pub use self::traits::HostedProject;
pub use self::traits::HostingService;
pub use self::traits::HostingServiceError;

pub use self::types::Award;
pub use self::types::CheckStatus;
pub use self::types::Comment;
pub use self::types::Commit;
pub use self::types::CommitStatus;
pub use self::types::CommitStatusState;
pub use self::types::Issue;
pub use self::types::MergeRequest;
pub use self::types::PendingCommitStatus;
pub use self::types::Repo;
pub use self::types::User;
