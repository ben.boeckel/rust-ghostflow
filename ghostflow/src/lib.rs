// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![warn(missing_docs)]
#![recursion_limit = "128"]

//! Ghostflow
//!
//! This crate implements actions which are part of ghostflow, the git-hosted development workflow.
//! This includes things such as applying hook checks to a commit, merging branches, handling
//! testing commands, and so on. See the documentation for the various actions for more
//! information.

pub mod actions;
pub mod host;
pub mod utils;

#[cfg(test)]
mod tests;
